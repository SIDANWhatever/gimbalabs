import React from "react";

export default function Governance(props) {
  return (
    <div className="bee-cursor h-full bg-black text-offWhite p-5">
      <h1 className="text-6xl">Gimbalabs Governance Sessions</h1>
      <p className="text-xl text-yellow-500 font-bold py-5">Starting January 10</p>
      <p className="font-semibold">Click for details</p>
    </div>
  );
}
